using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant {
    
    // the scene names
    public struct scene
    {
        public const  string GAMEPLAY = "Complete-Game";
        public const  string MAIN = "Init-Game";
        public const  string OPTIONS = "Options-Scene";
    }

    public struct parameter
    {
        public const string NUMBER_OF_PLAYERS = "NumberOfPlayers";
        public const string MAIN_CAMERA = "Main Camera";
    }
    // number of elements
    public struct amount
    {
        public const int NUMBER_OF_PLAYERS = 1;
        public const int MAX_NUMBER_OF_PLAYERS = 4;
    }
}
