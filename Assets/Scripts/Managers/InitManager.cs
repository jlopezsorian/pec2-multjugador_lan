using System;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Handles the init scene
 */
public class InitManager : MonoBehaviour
{
    private NetworkManager manager;
    [SerializeField] private string serverIP = "localhost";
    public void Awake()
    {
        new GameData();
        manager = FindObjectOfType<NetworkManager>();
    }
    
    public void Start()
    {
        CreateGame();
    }

    public void CreateGame() {
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.StartHost();
            }
        }
      //  AddressData();
    }
    
    public void JoinGame() {
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.networkAddress = serverIP;
                manager.StartClient();
            }
        }
       // AddressData();
    }
    
    public void RunServer() {
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.StartServer();
            }
        }
       // AddressData();
    }
    
    private void AddressData() {
        if (NetworkServer.active) {
            Debug.Log ("Server: active. IP: " + manager.networkAddress + " - Transport: " + Transport.activeTransport);
        }
        else {
            Debug.Log ("Attempted to join server " + serverIP);
        }

        Debug.Log ("Local IP Address: " + GetLocalIPAddress());

        Debug.Log ("//////////////");
    }

    private static string GetLocalIPAddress() {
        var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
        foreach (var ip in host.AddressList) {
            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                return ip.ToString();
            }
        }

        throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }

    //Quit the game
    public void ExitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }
}
