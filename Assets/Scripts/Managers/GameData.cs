using UnityEngine;
/**
 * Handles the number of player active in the game
 */
public class GameData
{
    public static int NumberOfPlayersPrefs
    {
        get => PlayerPrefs.GetInt(Constant.parameter.NUMBER_OF_PLAYERS);
        set => PlayerPrefs.SetInt(Constant.parameter.NUMBER_OF_PLAYERS, value);
    }

    public static int addOneNumberOfPlayersPrefs()
    {
        GameData.NumberOfPlayersPrefs = GameData.NumberOfPlayersPrefs+1;
        return GameData.NumberOfPlayersPrefs;
    }
    
    public static int subtractOneNumberOfPlayersPrefs()
    {
        GameData.NumberOfPlayersPrefs = GameData.NumberOfPlayersPrefs-1;
        return GameData.NumberOfPlayersPrefs;
    }

    static GameData()
    {
        if (PlayerPrefs.GetInt(Constant.parameter.NUMBER_OF_PLAYERS) == 0)
        {
            PlayerPrefs.SetInt(Constant.parameter.NUMBER_OF_PLAYERS, Constant.amount.NUMBER_OF_PLAYERS);
        }
    }

}
