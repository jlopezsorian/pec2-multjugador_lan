using UnityEngine;
using Mirror;

public class EnemySpawner : NetworkBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private int numberOfEnemies;
    [SerializeField] private Transform m_SpawnPoint;                          // The position and direction the tank will have when it spawns
    [SerializeField] private Color color;  


    public override void OnStartServer()
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
           /* var spawnPosition = new Vector3(
                Random.Range(-8.0f, 8.0f),
                0.0f,
                Random.Range(-8.0f, 8.0f));

            var spawnRotation = Quaternion.Euler(
                0.0f,
                Random.Range(0, 180),
                0.0f);
                */

            GameObject enemy = Instantiate(enemyPrefab, m_SpawnPoint.position, m_SpawnPoint.rotation);
            foreach (MeshRenderer child in enemy.GetComponentsInChildren<MeshRenderer>()) {
                child.material.color = color;
            }
            NetworkServer.Spawn(enemy);
        }
    }
}
